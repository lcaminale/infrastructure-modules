variable "env" {
  description = "Name of the environment. Example: prod"
  type        = "string"
}

variable "region" {
  description = "The aws region the resources will be build"
  type        = string
  default     = "eu-west-1"
}

variable "s3_bucket" {
  description = "S3 bucket for terraform state."
  type        = "string"
}

variable "tag_s3_bucket_name" {
  description = "Name tag for S3 bucket with terraform state."
  type        = "string"
}

variable "dynamodb_table" {
  description = "DynamoDB table name for terraform lock."
  type        = "string"
}
