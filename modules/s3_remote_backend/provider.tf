provider "aws" {
  region = var.region

  # Live modules pin exact provider version; generic modules let consumers pin the version.
  version = "~> 2.9"
}
