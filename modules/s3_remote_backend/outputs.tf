output "bucket_name" {
  value = aws_s3_bucket.backend_terraform.bucket
}

output "dynamo_table_name" {
  value = aws_dynamodb_table.terraform.name
}
