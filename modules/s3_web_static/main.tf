resource "aws_s3_bucket" "s3_bucket" {
  bucket = var.s3_bucket_name
  acl    = "public-read"
  policy = data.template_file.policy_json.rendered

  website {
    index_document = var.index_document
  }
  depends_on = [data.template_file.policy_json]
}


data "template_file" "policy_json" {
  template = file("${path.module}/policy.json.tmpl")
  vars = {
    bucket_arn = "arn:aws:s3:::${var.s3_bucket_name}/*"
  }
}

output "policy_json" {
  value = data.template_file.policy_json.rendered
}