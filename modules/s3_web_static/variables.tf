variable "region" {
  description = "Region aws"
  type = "string"
  default = "eu-west-1"
}

variable s3_bucket_name {
  type = "string"
  description = "Name of your bucket"
}

variable index_document {
  type = "string"
  default = "index.html"
  description = "Entry point of your html"
}
