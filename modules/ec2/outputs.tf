output "ec2_public_ip" {
  description = "The ip of the machine"
  value       = aws_instance.my-ec2.public_ip
  depends_on = [aws_instance.my-ec2]
}

output "ec2_public_dns" {
  description = "The ip of the machine"
  value       = aws_instance.my-ec2.public_dns
  depends_on = [aws_instance.my-ec2]
}

output "ssh_connect_cli" {
  description = "The command to enter for ssh connect to the machine"
  value       = "ssh -i ~/.ssh/<SSH_PUBLIC_KEY_PATH> <YOUR_USER>@${aws_instance.my-ec2.public_ip}"
  depends_on = [aws_instance.my-ec2]
}

output "arn" {
  description = "Arn of the EC2 instance"
  value = aws_instance.my-ec2.arn
  depends_on = [aws_instance.my-ec2]
}